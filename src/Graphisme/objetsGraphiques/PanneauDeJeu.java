package Graphisme.objetsGraphiques;
import javax.swing.JPanel;
import java.awt.Graphics;

public class PanneauDeJeu extends JPanel {
    private ModuleGraphique moduleGraphique;

    public PanneauDeJeu(ModuleGraphique moduleGraphique) {
        this.moduleGraphique = moduleGraphique;
        this.setFocusable(true);
        this.requestFocus();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        moduleGraphique.dessiner(g);
    }
}
