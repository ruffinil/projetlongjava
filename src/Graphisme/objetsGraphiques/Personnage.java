package Graphisme.objetsGraphiques;
public class Personnage extends Combattant {
	
	public Personnage() {
		super();
	}
	
    public Personnage(int vivalite, String nom) {
		super(nom, vivalite);
	}

	@Override
	public void attaquer(CombattantInterface adversaire)
    {
   	 adversaire.defendre(this.classe.getAttaque().debuterAttaque(this, adversaire));
    }

	@Override
	public String toString() {
		return nom + ": Vivalité=" + vivalite;
	}
}
