package Graphisme.objetsGraphiques;
import *;
import java.util.Random;

public class DefendreParade implements IDefense {

    // La classe DefendreParade représente une méthode de défense 
    // qui évalue les chances de parer une attaque reçue. 
	
	private String nom;
	private String definition;
	private double parade;
	
	
	/**
	 * Constructeur de DefendreParade
	 * @param nom
	 * @param definition
	 * @param parade
	 */
	public DefendreParade(String nom, String definition, double parade) {
		super();
		this.nom = nom;
		this.definition = definition;
		this.parade = parade;
	}

	public String getNom() {
		return this.nom;
	}

	public String getDefinition() {
		return this.definition;
	}

	public String information() {
		return null;
	}

	/**
	 * évalue les chances de parade de l'attaque reçue
	 * @param degats  
	 * @return 0 si attaque parée sinon les degats.
	 */
	public int defense(int degats) {
        int chance = new Random().nextInt(100);
		return this.parade*100 > chance ? 0 : degats;
	}

	public double getParade() {
		return parade;
	}

	public void setParade(double parade) {
		this.parade = parade;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}
	
	

}
