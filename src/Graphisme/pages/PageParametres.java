package pages;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;
import main.Jeu;

public class PageParametres extends VBox {
    public PageParametres() {
        super(20);
        this.setAlignment(Pos.CENTER);

        Button boutonRetour = new Button("Retour");

        // Ajuster la taille du bouton
        boutonRetour.setMinSize(200, 50);

        boutonRetour.setOnAction(e -> PageMenuPrincipal.demarer());

        this.getChildren().add(boutonRetour);

        Scene sceneParametres = new Scene(this, 800, 600);
        Jeu.getBaseGraphique().setScene(sceneParametres);
        Jeu.getBaseGraphique().setTitle("Paramètres");
    }

    public static void demarer() {
        new PageParametres();
    }
}
