package pages;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import main.Jeu;

public class PageNiveau extends VBox {
    public PageNiveau() {
        super(20);
        this.setAlignment(Pos.CENTER);

        // Liste de niveaux (exemple)
        String[] niveaux = { "Niveau 1", "Niveau 2", "Niveau 3" };

        for (String niveau : niveaux) {
            Button button = new Button(niveau);
            button.setMinSize(200, 50);
            button.setOnAction(e -> PageJeu.demarer(niveau));
            this.getChildren().add(button);
        }

        Button boutonRetour = new Button("Retour");
        boutonRetour.setMinSize(200, 50);
        boutonRetour.setOnAction(e -> PageMenuPrincipal.demarer());
        this.getChildren().add(boutonRetour);

        Scene sceneNiveau = new Scene(this, 800, 600);
        Jeu.getBaseGraphique().setScene(sceneNiveau);
        Jeu.getBaseGraphique().setTitle("Choix de Niveau");
    }

    public static void demarer() {
        new PageNiveau();
    }
}
