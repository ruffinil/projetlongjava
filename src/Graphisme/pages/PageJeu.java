package pages;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import main.Jeu;

public class PageJeu extends VBox {
    public PageJeu(String niveau) {
        super(20);
        this.setAlignment(Pos.CENTER);

        Label label = new Label("En cours de jeu: " + niveau);
        label.setStyle("-fx-font-size: 24pt;");

        Button boutonRetour = new Button("Retour");
        boutonRetour.setMinSize(200, 50);
        boutonRetour.setOnAction(e -> PageMenuPrincipal.demarer());

        this.getChildren().addAll(label, boutonRetour);

        Scene sceneJeu = new Scene(this, 800, 600);
        Jeu.getBaseGraphique().setScene(sceneJeu);
        Jeu.getBaseGraphique().setTitle("Jeu - " + niveau);
    }

    public static void demarer(String niveau) {
        new PageJeu(niveau);
    }
}
