package pages;

import ObjetsCombats.*;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.layout.StackPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.util.ArrayList;
import java.net.URL;
import javafx.scene.control.Label;
import main.Jeu;

public class PageAmelioration extends VBox {
    private int[] bonus = new int[3]; // Tableaux de bonus pour atk, def, etc.

    public PageAmelioration() {
        super(20);
        this.setAlignment(Pos.CENTER);

        ObjetsCombatInit objetsInit = new ObjetsCombatInit();
        ArrayList<ObjetsCombat> objets = objetsInit.getObjs();

        for (int i = 0; i < objetsInit.getTaille(); i++) {
            int index = i;
            Button button = new Button();
            button.setOnAction(event -> {
                if (objets.get(index).getTypeAttaque()) {
                    bonus[1] = objets.get(index).getBonus();
                } else {
                    bonus[2] = objets.get(index).getBonus();
                }
            });

            String imagePath = "/ObjetsCombats/Objets/" + objets.get(index).getImPath();
            URL imageURL = getClass().getResource(imagePath);
            if (imageURL == null) {
                System.err.println("Ressource non trouvée : " + imagePath);
                continue;
            }
            Image image = new Image(imageURL.toExternalForm());
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth(100);
            imageView.setFitHeight(100);
            StackPane stack = new StackPane();
            Label label = new Label("+ " + objets.get(index).getBonus() + (objets.get(index).getTypeAttaque() ? " atk" : " def"));
            label.setStyle("-fx-font-weight: bold; -fx-font-size: 16pt;");
            stack.getChildren().addAll(imageView, label);
            button.setGraphic(stack);
            this.getChildren().add(button);
        }

        Button boutonRetour = new Button("Retour");
        boutonRetour.setMinSize(200, 50);
        boutonRetour.setOnAction(e -> PageMenuPrincipal.demarer());
        this.getChildren().add(boutonRetour);

        Scene sceneAmelioration = new Scene(this, 800, 600);
        Jeu.getBaseGraphique().setScene(sceneAmelioration);
        Jeu.getBaseGraphique().setTitle("Amélioration");
    }

    public static void demarer() {
        new PageAmelioration();
    }
}
