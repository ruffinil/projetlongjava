package pages;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import main.Jeu;

public class PageDemarage extends VBox{
    
    static PageDemarage page = null;

    private PageDemarage(){
        super(20);
        // on place les elements 
        Button boutonDemarer = new Button("démarer");
        
        boutonDemarer.setOnMouseClicked(event -> actionBoutonDemarer());

        Button boutonCredits = new Button("crédits");
       
        this.setAlignment(Pos.CENTER);
        this.getChildren().addAll(boutonDemarer,boutonCredits);
        //on paramettre la fenetre de demarage
        Scene ecranDemarage = new Scene(this);
        Stage baseGraphique = Jeu.getBaseGraphique();
        baseGraphique.setScene(ecranDemarage);
        baseGraphique.setTitle("Bienvenue");
        baseGraphique.setWidth(640);
        baseGraphique.setHeight(480);

    }
    public static void demarer(){
        if (page!=null){
            close();// on redemare la fenêtre
        }
        page = new PageDemarage();
    }
    public static void close() {
        throw new UnsupportedOperationException("Unimplemented method 'close'");
    }
    private void actionBoutonDemarer(){
        PageMenuPrincipal.demarer();
    }


}
