package pages;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;
import main.Jeu;

public class PageMenuPrincipal extends VBox {
    static PageMenuPrincipal page = null;

    private PageMenuPrincipal() {
        super(20); // Espacement entre les boutons
        this.setAlignment(Pos.CENTER);

        Button boutonQuitter = new Button("Quitter");
        Button boutonJouer = new Button("Jouer");
        Button boutonParametres = new Button("Paramètres");
        Button boutonNiveau = new Button("Niveau");
        Button boutonAmelioration = new Button("Amélioration");

        // Ajuster la taille des boutons
        boutonQuitter.setMinSize(200, 50);
        boutonJouer.setMinSize(200, 50);
        boutonParametres.setMinSize(200, 50);
        boutonNiveau.setMinSize(200, 50);
        boutonAmelioration.setMinSize(200, 50);

        // Ajouter les actions aux boutons
        boutonQuitter.setOnAction(e -> System.exit(0));
        boutonJouer.setOnAction(e -> PageJeu.demarer("Niveau 1"));
        boutonParametres.setOnAction(e -> ouvrirParametres());
        boutonNiveau.setOnAction(e -> choisirNiveau());
        boutonAmelioration.setOnAction(e -> ameliorerEquipement());

        // Ajouter les boutons au VBox
        this.getChildren().addAll(boutonJouer, boutonParametres, boutonNiveau, boutonAmelioration, boutonQuitter);

        Scene ecranMenuPrincipal = new Scene(this, 800, 600);
        Jeu.getBaseGraphique().setScene(ecranMenuPrincipal);
        Jeu.getBaseGraphique().setTitle("Menu Principal");
    }

    private void ouvrirParametres() {
        PageParametres.demarer();
    }

    private void choisirNiveau() {
        PageNiveau.demarer();
    }

    private void ameliorerEquipement() {
        PageAmelioration.demarer();
    }

    public static void demarer() {
        page = new PageMenuPrincipal();
    }

    public static void close() {
        throw new UnsupportedOperationException("Unimplemented method 'close'");
    }
}
