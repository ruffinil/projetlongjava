package Menus;
import Son.LecteurAudio;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;
import javafx.scene.control.Slider;
import javafx.scene.control.Button;

public class MenuParametre {
    
    public void afficherParametre(Stage stage, LecteurAudio son, Scene scene){
        VBox menuParametre = new VBox();
        Slider sliderSon = new Slider(0, 100 , 5);
        sliderSon.setValue(son.getVolume());
        Scene sceneParam = new Scene(menuParametre,800, 400);
        Button save = new Button("Sauvegarder");
        Button retour = new Button("retour");
        retour.setOnAction(event ->  stage.setScene(scene));
        save.setOnAction(event -> son.setVolume(sliderSon.getValue()));
        menuParametre.getChildren().add(sliderSon);
        menuParametre.getChildren().add(save);
        menuParametre.getChildren().add(retour);
        stage.setScene(sceneParam);
        
    }
}
