package Menus;
import ObjetsCombats.ObjetsCombat;
import Son.LecteurAudio;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class MenuDemarrer extends Application {
    private LecteurAudio son = new LecteurAudio(0, 0, 0);
    private MenuParametre params = new MenuParametre();
    @Override
    public void start(Stage stage) {
        
        MenuObjets menuObjets = new MenuObjets();
        Button parametre = new Button("Paramètres");
        VBox menuPrincipal = new VBox();
        menuPrincipal.getChildren().add(parametre);
        Button objets = new Button("Objets");
        menuPrincipal.getChildren().add(objets);
        ObjetsCombat[] bonus = new ObjetsCombat[2];
        Button quitter = new Button("Quitter");
        quitter.setOnAction(event -> stage.close());
        menuPrincipal.getChildren().add(quitter);
        Scene scene = new Scene(menuPrincipal, 800, 400);
        objets.setOnAction(event -> {
            if (menuObjets.isLoaded()) {
                menuObjets.loadedAffichage(stage);
            } else {
                menuObjets.afficherObjs(stage, bonus, scene);
            }
        });
        parametre.setOnAction(event -> params.afficherParametre(stage,son , scene));
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}