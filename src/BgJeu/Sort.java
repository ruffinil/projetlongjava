package evenement;
public interface Sort {
    
    
    //Cette interface représente un sort dans le jeu comme 
    //le sort de guérison et de defense.

	public String getNom();

	public String getDefinition();
	
	public String information();
}
