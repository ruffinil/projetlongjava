package Son;
//package //nom du package;

//import //nom du module contenant les sons;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public final class LecteurAudio {
    private static final Logger journal = LoggerFactory.getLogger(LecteurAudio.class);
    private MediaPlayer lecteur;
    private double volume;
    private double rythme; // Temps en millisecondes entre chaque battement
    private double tolérance; // Tolérance en millisecondes pour appuyer sur le bouton en synchronisation avec le rythme
    private boolean dansLeRythme; // Indique si le joueur appuie dans le rythme
    private int points;

    public LecteurAudio(float volume, double rythme, double tolérance) {
        this.volume = volume;
        this.rythme = rythme;
        this.tolérance = tolérance;
        this.points = 0;
    }

    public void charger(String chemin) {
        charger(new Media(new File(chemin).toURI().toString()));
        journal.info("Piste changée : " + chemin + ", volume : " + volume + ".");
    }

    public void charger(Media media) {
        lecteur = new MediaPlayer(media);
        lecteur.setVolume(volume);
        lecteur.setOnStopped(lecteur::dispose);
        lecteur.setOnEndOfMedia(lecteur::dispose);
        journal.info("Piste changée : " + media.getSource() + ", volume : " + volume);
    }

    public void chargementRapide(String chemin, boolean boucle) {
        arreter();
        charger(chemin);
        if(boucle) {
            boucle();
        }
        jouer();
    }

    public void jouer() {
        lecteur.setOnReady(() -> {
            lecteur.play();
            commencerDétectionRythme();
        });
    }

    public void lire(String identifiant) {
        if(identifiant.equals("coup-de-piste")) {
            return;
        }
        Sound.getAudioClip(identifiant).play(volume);
        if (dansLeRythme) {
            points++;
            // Augmenter les points du joueur si le bouton est pressé dans le rythme
        }
    }

    public void commencerDétectionRythme() {
        dansLeRythme = false;
        lecteur.setOnPlaying(() -> {
            new Thread(() -> {
                while (lecteur.getStatus() == MediaPlayer.Status.PLAYING) {
                    double tempsActuel = lecteur.getCurrentTime().toMillis() % rythme;
                    if (tempsActuel < tolérance || rythme - tempsActuel < tolérance) {
                        dansLeRythme = true;
                    } else {
                        dansLeRythme = false;
                    }
                    try {
                        Thread.sleep(10); // Vérifie le rythme toutes les 10 millisecondes
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        });
    }

    public void pause() {
        lecteur.pause();
    }

    public void reprendre() {
        lecteur.play();
    }

    public void arreter() {
        lecteur.stop();
    }

    public void boucle() {
        lecteur.setOnEndOfMedia(() -> lecteur.seek(Duration.millis(0)));
    }

    public double getVolume() {
        return lecteur.getVolume();
    }

    public void setVolume(double volume) {
        this.volume = volume;
        lecteur.setVolume(volume);
    }

    public int getPoints() {
        return points;
    }
}
