package Son;

import java.io.IOException;
import java.util.Scanner;
import javax.sound.sampled.*;

public class ExempleMainSon {


    public static void main (String[] args) {
        try {

            InterfaceJeu IJ = new InterfaceJeu(args[0]);

            String reponse = "";
            Scanner scanner = new Scanner(System.in);

            while(!reponse.equals("Q")) {

                System.out.println("Quelle piste ?");
                System.out.println("Ton choix : ");

                int piste = Integer.valueOf(scanner.next());

                System.out.println("J = jouer, S = Stop, Q = Quitter");
                System.out.print("Ton choix : ");
                
                reponse = scanner.next();
                reponse = reponse.toUpperCase();
                
                switch(reponse) {
                    case ("J") : IJ.jouerMusique(piste);
                    break;
                    case ("S") : IJ.stopMusique(piste);
                    break;
                    case ("Q") : IJ.fermerPiste(piste);
                    break;
                    default: System.out.println("Invalide");
                }
                
            }
            scanner.close();
            
        } catch (UnsupportedAudioFileException e) {
            System.out.println("Format audio non supporté : " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Erreur d'entrée/sortie : " + e.getMessage());
        } catch (LineUnavailableException e) {
            System.out.println("Ligne audio non disponible : " + e.getMessage());
        }
    }
}
