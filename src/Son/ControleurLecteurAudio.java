package Son;
package //nom du package;

import java.util.HashMap;

public class ControleurLecteurAudio {

    private static HashMap<String, LecteurAudio> lecteursAudio = new HashMap<>();

    public ControleurLecteurAudio() {
        ajouterLecteurAudio("musique", new LecteurAudio(0.3f));
        ajouterLecteurAudio("effet", new LecteurAudio(0.2f));
    }

    public void ajouterLecteurAudio(String nom, LecteurAudio lecteurAudio) {
        lecteursAudio.put(nom, lecteurAudio);
    }

    public static LecteurAudio getLecteurAudio(String lecteur) {
        return lecteursAudio.get(lecteur);
    }
}
