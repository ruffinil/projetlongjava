package Son;

import java.io.IOException;
import javax.sound.sampled.*;

public class InterfaceJeu {

        Son son;

        public InterfaceJeu(String nomBgMusique) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
            son = new Son(nomBgMusique);
        }

        /**
         * Jouer de la musique
         * @param i l'index de la musique (voir dossierSons)
         */
        public void jouerMusique(int i) {
            son.jouerMusique(i);
        }

        public void stopMusique(int i) {

            son.stop(i);
        }

        public void jouerMusiqueMenu(int i) {

            son.jouerMusique(i);
            son.boucle(i);
        }

        public void rejouer(int i) {

            son.rejouer(i);
        }

        public void fermerPiste(int i) {

            son.fermer(i);
        }


    }