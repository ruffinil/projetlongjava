import javafx.stage.Stage;
import pages.PageDemarage;
import javafx.application.Application;
/*La classe principale du jeu
 */
public class Jeu extends Application {
    private static Stage baseGraphique;
    public static Stage getBaseGraphique() {
        return baseGraphique;
    }

    /*
     * Le point d'entré du jeu
     */
    @Override
    public void start(Stage baseGraphique) throws Exception {
        Jeu.baseGraphique = baseGraphique;
         // on affiche la fenetre
        baseGraphique.show();
        PageDemarage.demarer();
    }



    /** Fonction dédiée au javaFX pour les cas d'erreurs
     * @param args
     */
    public static void main(String[] args){
        launch(args);
    }


}
